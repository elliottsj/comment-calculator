//
//  SCRCalculator.m
//  Scumbag Calculator
//
//  Created by Spencer Elliott on 2013-02-17.
//  Copyright (c) 2013 Spencer Elliott. All rights reserved.
//

#import "CCRCalculator.h"

@interface CCRCalculator ()

@property (strong, nonatomic) NSMutableArray *programStack;

@end


@implementation CCRCalculator

- (id)program
{
    return [self.programStack copy];
}

- (id)programStack {
    if (!_programStack)
        _programStack = [[NSMutableArray alloc] init];
    return _programStack;
}

- (BOOL)lastElementIsOperation {
    return [[self.class operations] containsObject:self.programStack.lastObject];
}

- (void)pushOperand:(double)operand
{
    [self.programStack addObject:[NSNumber numberWithDouble:operand]];
}

- (void)pushOperation:(NSString *)operation
{
    if (!self.programStack.count)
        @throw [[NSException alloc] initWithName:@"EmptyStackException"
                                          reason:@"There are no operands in the stack."
                                        userInfo:nil];

    if (![[self.class operations] containsObject:operation])
        @throw [[NSException alloc] initWithName:@"InvalidOperationException"
                                          reason:@"An invalid operation was pushed."
                                        userInfo:nil];
    
    if ([[self.class operations] containsObject:self.programStack.lastObject])
        @throw [[NSException alloc] initWithName:@"InvalidOperationException"
                                          reason:@"An invalid operation was pushed"
                                        userInfo:nil];
    
    [self.programStack addObject:operation];
}

- (void)pop
{
    if ([self.programStack count] != 0){
        [self.programStack removeLastObject];
    }
}

- (double)run {
    return [self.class runProgram:self.programStack];
}

- (void)clear
{
    [self.programStack removeAllObjects];
}

- (NSString *)description
{
    return [self.class descriptionOfProgram:self.programStack];
}

+ (NSSet *)operations
{
    return [[self priorityOperations] setByAddingObjectsFromArray:@[@"+", @"−", @"-"]];
}

+ (NSSet *)priorityOperations {
    return [NSSet setWithObjects:@"×", @"*", @"÷", @"/", nil];
}

+ (double)runProgram:(id)program {
    if ([program count] == 0 || [[self operations] containsObject:[program lastObject]])
        return 0.0;
    if ([program count] == 1)
        return [program[0] doubleValue];
    NSString *operation = program[1];
    if ([[self priorityOperations] containsObject:operation]) {
        double result = 0.0;
        if ([operation isEqualToString:@"*"] || [operation isEqualToString:@"×"])
            result = [program[0] doubleValue] * [program[2] doubleValue];
        else if ([operation isEqualToString:@"/"] || [operation isEqualToString:@"÷"])
            result = [program[0] doubleValue] / [program[2] doubleValue];
        NSArray *nextProgram = [program subarrayWithRange:NSMakeRange(3, [program count] - 3)];
        return [self runProgram:[@[@(result)] arrayByAddingObjectsFromArray:nextProgram]];
    } else if ([[self operations] containsObject:operation]) {
        NSArray *nextProgram = [program subarrayWithRange:NSMakeRange(2, [program count] - 2)];
        if ([operation isEqualToString:@"+"])
            return [program[0] doubleValue] + [self runProgram:nextProgram];
        else if ([operation isEqualToString:@"−"] || [operation isEqualToString:@"−"])
            return [program[0] doubleValue] - [self runProgram:nextProgram];
    }
    return 0.0;
}

+ (NSString *)descriptionOfProgram:(id)program
{
    return @""; // placeholder
}

@end