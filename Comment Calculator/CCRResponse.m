//
//  SCRDiss.m
//  Scumbag Calculator
//
//  Created by Spencer Elliott on 2013-02-17.
//  Copyright (c) 2013 Spencer Elliott. All rights reserved.
//

#import "CCRResponse.h"

@interface CCRResponse ()

@property (weak, nonatomic) NSDictionary *disses;

+ (NSDictionary *)dissesFromFile;

@end


@implementation CCRResponse

- (id)initFromNumber:(NSNumber *)number
{
    self = [super init];
    [self setDisses:[self.class dissesFromFile]];
    NSArray *dissArray;
    if ([number isEqualToNumber:@0]) {
        dissArray = [[self disses] objectForKey:@"zero"];
    } else if ([number isEqualToNumber:@1]) {
        dissArray = [[self disses] objectForKey:@"one"];
    } else if ([number compare:@10] == NSOrderedAscending) {
        dissArray = [[self disses] objectForKey:@"small"];
    } else if ([number compare:@100] == NSOrderedAscending) {
        dissArray = [[self disses] objectForKey:@"medium"];
    } else if ([number compare:@1000] == NSOrderedAscending) {
        dissArray = [[self disses] objectForKey:@"large"];
    } else {
        dissArray = [[self disses] objectForKey:@"verylarge"];
    }
    _diss = [self.class randomDissFromArray:dissArray];
    return self;
}

- (NSString *)description
{
    return self.diss;
}

+ (id)dissFromNumber:(NSNumber *)number
{
    return [[self alloc] initFromNumber:number];
}

+ (NSString *)randomDissFromArray:(NSArray *)array
{
    return [array objectAtIndex:arc4random_uniform([array count])];
}

+ (NSDictionary *)dissesFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Responses" ofType:@"plist"];
    return [NSDictionary dictionaryWithContentsOfFile:path];
}

@end
