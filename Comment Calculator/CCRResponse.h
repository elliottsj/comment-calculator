//
//  SCRDiss.h
//  Scumbag Calculator
//
//  Created by Spencer Elliott on 2013-02-17.
//  Copyright (c) 2013 Spencer Elliott. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCRResponse : NSObject

@property (weak, readonly, nonatomic) NSString *diss;

- (id)initFromNumber:(NSNumber *)number;
- (NSString *)description;

+ (id)dissFromNumber:(NSNumber *)number;

@end
