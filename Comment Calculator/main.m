//
//  main.m
//  Scumbag Calculator
//
//  Created by Spencer Elliott on 2013-02-13.
//  Copyright (c) 2013 Spencer Elliott. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCRAppDelegate class]));
    }
}
