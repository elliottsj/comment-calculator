//
//  SCRViewController.m
//  Scumbag Calculator
//
//  Created by Spencer Elliott on 2013-02-13.
//  Copyright (c) 2013 Spencer Elliott. All rights reserved.
//

#import "CCRViewController.h"
#import "CCRResponse.h"
#import "CCRCalculator.h"

@interface CCRViewController ()

@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *displayBackground;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *digitButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *operatorButtons;
@property (weak, nonatomic) IBOutlet UIButton *equalsButton;
@property (weak, nonatomic) IBOutlet UILabel *mainDisplay;
@property (weak, nonatomic) IBOutlet UILabel *logDisplay;
@property (strong, nonatomic) CCRCalculator *calculator;
@property (strong, nonatomic) NSMutableString *currentOperand;
@property (strong, nonatomic) CCRResponse *response;
@property (nonatomic) BOOL currentOperandHasDecimal;
@property (nonatomic) BOOL userIsEnteringAnExpression;

@end

@implementation CCRViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *background = [UIImage imageNamed:@"background.png"];
    
    UIImage *displayBackground = [[UIImage imageNamed:@"displayBackground.png"]
                                  resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    
    UIImage *blueButton = [[UIImage imageNamed:@"blueButton.png"]
                           resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *blueButtonHighlight = [[UIImage imageNamed:@"blueButtonHighlight.png" ]
                                    resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    
    UIImage *blackButton = [[UIImage imageNamed:@"blackButton.png"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *blackButtonHighlight = [[UIImage imageNamed:@"blackButtonHighlight.png" ]
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    
    UIImage *orangeButton = [[UIImage imageNamed:@"orangeButton.png"]
                             resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *orangeButtonHighlight = [[UIImage imageNamed:@"orangeButtonHighlight.png" ]
                                      resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:background];
    self.displayBackground.image = displayBackground;
    
    for (UIButton *button in self.digitButtons) {
        [button setBackgroundImage:blackButton forState:UIControlStateNormal];
        [button setBackgroundImage:blackButtonHighlight forState:UIControlStateHighlighted];
    }

    for (UIButton *button in self.operatorButtons) {
        [button setBackgroundImage:blueButton forState:UIControlStateNormal];
        [button setBackgroundImage:blueButtonHighlight forState:UIControlStateHighlighted];
    }
    
    [self.equalsButton setBackgroundImage:orangeButton forState:UIControlStateNormal];
    [self.equalsButton setBackgroundImage:orangeButtonHighlight forState:UIControlStateHighlighted];
}

- (id)calculator {
    if (!_calculator)
        _calculator = [[CCRCalculator alloc] init];
    return _calculator;
}

- (id)currentOperand {
    if (!_currentOperand)
        _currentOperand = [[NSMutableString alloc] init];
    return _currentOperand;
}

- (id)response {
    if (!_response)
        _response = [[CCRResponse alloc] init];
    return _response;
}

- (BOOL)currentOperandHasDecimal {
    return [self.currentOperand rangeOfString:@"."].location != NSNotFound;
}

- (IBAction)digitPressed:(UIButton *)sender {        
    NSString *digit = [sender currentTitle];
    if (!self.userIsEnteringAnExpression) {
        self.currentOperand = [digit mutableCopy];
        self.logDisplay.text = digit;
        self.userIsEnteringAnExpression = YES;
    } else {
        [self.currentOperand appendString:digit];
        self.logDisplay.text = [self.logDisplay.text stringByAppendingString:digit];
    }
}

- (IBAction)operationPressed:(UIButton *)sender {
    if (!self.userIsEnteringAnExpression) {
        // Use result from main display as beginning of expression.
        self.currentOperand.string = self.mainDisplay.text;
        self.logDisplay.text = self.currentOperand;
        self.userIsEnteringAnExpression = YES;
    }
    [self pushOperand];
    @try {
        [self.calculator pushOperation:sender.currentTitle];
        self.logDisplay.text = [self.logDisplay.text stringByAppendingFormat:@" %@ ", sender.currentTitle];
    }
    @catch (NSException *exception) {
    }
}

- (IBAction)equalsPressed {
    if (!self.currentOperand.length) {
        [self.currentOperand appendString:self.mainDisplay.text];
        self.logDisplay.text = [self.logDisplay.text stringByAppendingString:self.currentOperand];
    }
    [self pushOperand];
    NSNumber *result = @([self.calculator run]);
    self.mainDisplay.text = [NSString stringWithFormat:@"%@", result];
    
    [[[UIAlertView alloc]initWithTitle:self.mainDisplay.text
                               message:[[CCRResponse dissFromNumber:result] description]
                              delegate:nil
                     cancelButtonTitle:@"I know"
                     otherButtonTitles:nil] show];
    
    if (self.userIsEnteringAnExpression) {
        self.userIsEnteringAnExpression = NO;
        [self.calculator clear];
    }
}

- (void)pushOperand {
    if ([self.currentOperand length]) {
        [self.calculator pushOperand:[self.currentOperand doubleValue]];
        self.currentOperand.string = @"";
    }
}

- (IBAction)decimalPressed {
    if (!self.currentOperandHasDecimal) {
        [self.currentOperand appendString:@"."];
        self.logDisplay.text = [self.logDisplay.text stringByAppendingString:@"."];
    }
}

- (IBAction)clearPressed {
    self.logDisplay.text = @"";
    self.mainDisplay.text = @"0";
    [self.currentOperand setString:@""];
    self.userIsEnteringAnExpression = NO;
    [self.calculator clear];
}

- (IBAction)changeSignPressed {
    if (self.currentOperand.length) {
        NSString *originalOperand = [self.currentOperand copy];
        if ([[self.currentOperand substringToIndex:1] isEqualToString:@"−"])
            [self.currentOperand deleteCharactersInRange:NSMakeRange(0, 1)];
        else
            [self.currentOperand insertString:@"−" atIndex:0];
        self.logDisplay.text = [self.logDisplay.text stringByReplacingOccurrencesOfString:originalOperand
                                                                               withString:self.currentOperand
                                                                                  options:0
                                                                                    range:NSMakeRange(self.logDisplay.text.length - originalOperand.length, originalOperand.length)];
    }
}

@end
