//
//  SCRCalculator.h
//  Scumbag Calculator
//
//  Created by Spencer Elliott on 2013-02-17.
//  Copyright (c) 2013 Spencer Elliott. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSSet *const operations;

@interface CCRCalculator : NSObject

@property (weak, readonly, nonatomic) id program;
@property (readonly, nonatomic) BOOL lastElementIsOperation;

- (void)pushOperand:(double)operand;
- (void)pushOperation:(NSString *)operation;
- (void)clear;
- (double)run;
- (NSString *)description;

+ (double)runProgram:(id)program;
+ (NSString *)descriptionOfProgram:(id)program;

@end
